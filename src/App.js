import { Component } from 'react';
import './App.css';
import Footer from './components/footer';
import Slide from './components/slide';

class App extends Component {
  render() {
    const title         = "Welcome to the GALERIA";
    const description   = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident molestias minus rerum officia, maiores quas. Corrupti sit harum nam quae doloremque odio modi debitis? Magnam, enim cum? Iste, iure numquam!";
    
    return (
      <div className    = "App">
      <slide />
      
        <h1>{ title }</h1>
        
        <h2>{ description }</h2>

        <Footer />
      </div>
    )
  }

}




export default App;
