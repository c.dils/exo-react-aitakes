import React, { Component } from 'react'
import { Slide } from 'react-slideshow-image';
import img1 from '../img/img1.jpg';
import img2 from '../img/img2.jpg';
import img3 from '../img/img3.jpg';

const properties = [
  'img/img1.jpg',
  'img/img2.jpg',
  'img/img3.jpg'
];

const slideshow = () => {
  return (
    <div className= "containerSlide">
      <Slide {...properties}>
        <div className="each-slide">
          <div>
            <img src={img1} alt="image" />
          </div>
        </div>
        <div className="each-slide">
          <div>
          <img src={img2} alt="image" />
          </div>
        </div>
        <div className="each-slide">
          <div>
          <img src={img3} alt="image" />
          </div>
        </div>
      </Slide>
    </div>
  )
};

export default slideshow